**GenomeArtiFinder** is a collection of command line RScripts that use the variant allele frequency (VAF) distribution of germline heterozygous variants in matched tumour:normal samples to detect sample contamination and other abnormalities in sequencing data.

**Usage details are available in the [wiki](https://git.ecdf.ed.ac.uk/taylor-lab/GenomeArtiFinder/wikis/home).**

These tools are only suitable for use with paired tumour:normal whole exome and whole genome sequencing data from heterozygous (not inbred), diploid organisms. They are not suitable for use with data from smaller targeted sequencing panels or other data types (RNA-seq, microarray, etc).

The minimum data requirements to run the analysis are a normal sample BAM, a tumour sample BAM and a normal sample VCF of single nucleotide variants. 


**Reference:** Luft J, Young RS, Meynert AM, Taylor MS. 2020. [Detecting oncogenic selection through biased allele retention in The Cancer Genome Atlas.](https://www.biorxiv.org/content/10.1101/2020.07.03.186593v1) bioRxiv doi: 10.1101/2020.07.03.186593

**Contact:** juliet.luft@ed.ac.uk