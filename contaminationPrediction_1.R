#!/usr/bin/env Rscript
start <- Sys.time()
library("optparse")
library("MASS")

option_list = list(
  make_option(c("-i", "--input"), type="character", default=NULL,
              help="REQUIRED - Tab separated text file with list of input files - file path relative to cwd, format: \n                ID\tNORMAL VCF\t\tNORMAL BAM\tTUMOUR BAM\t", metavar="patientlist"),
        make_option(c("-s", "--src"), type="character", default=".",
              help="Directory containing source files. [default = cwd]", metavar="srcdir"),
        make_option(c("-d", "--dir"), type="character", default=".",
              help="Directory containing input files. [default = cwd]", metavar="datadir"),
        make_option(c("-o", "--out"), type="character", default=".",
              help="Output file directory. [default = cwd]", metavar="outdir"),
        make_option(c("-x", "--ExomeSeqMiner"), type="character", default=".",
              help="Location of the \"ExomeSeqMiner\" directory. [default = cwd]", metavar="exdir"),
        make_option(c("-j", "--jobid"), type="character", default=NULL,
              help="Specify job ID - defines optional output file prefix.", metavar="test"),
        make_option(c("-b", "--batchid"), type="character", default=NULL,
              help="Specify batch ID - defines optional output file suffix.", metavar="n"),
        make_option(c("-a", "--append"), action="store_true", default=FALSE,
              help="Append results to existing output file of same name."),
        make_option(c("-w", "--overwrite"), action="store_true", default=FALSE,
              help="Overwrite existing output file of same name - has priority over '--append'."),
        make_option(c("-p", "--plot"), action="store_true", default=FALSE,
              help="Plot VAF distribution of each sample - creates a new directory within the specified output directory: \"VAF-distribution-plots\"."),
        make_option(c("-v", "--verbose"), action="store_true", default=FALSE,
              help="Output job information as it runs.")
)

opt_parser = OptionParser(option_list=option_list, description = "Collects germline heterozygous VAF data from matched tumour:normal BAM files, performs variant filtering and consequently predicts the degree of non-self contamination.")
opt = parse_args(opt_parser)

if (is.null(opt$input)){
  stop(paste0("Input file not specified. See help for details. \"Rscript contaminationPrediction_2.R --help\" \n"))
}

if (opt$verbose == T){
  cat(paste0(Sys.time(), ": Preparing analysis.\n"))
}

if (!file.exists(paste0(opt$src, "/GRCh38.variant.inclist.tsv"))){
  stop(paste0("Cannot locate ", opt$src, "/GRCh38.variant.inclist.tsv - Check '--src' / '-s'. See help for details. \"Rscript contaminationPrediction.R --help\" \n"))
}
inclist <- read.table(paste0(opt$src, "/GRCh38.variant.inclist.tsv"), sep = "\t", header = T, stringsAsFactors = F)

if (sum(file.exists(paste0(opt$src, "/contaminationPredictionModel.Rdat"))) == 0){
  stop(paste0("Cannot locate ", opt$src, "/contaminationPredictionModel.Rdat - Check '--src' / '-s' input. See help for details. \"Rscript contaminationPrediction_2.R --help\" \n"))
}

if (sum(file.exists(paste0(opt$src, "/hg38_filtered.exomeTarget.sorted.bed"))) == 0){
  stop(paste0("Cannot locate ", opt$src, "/hg38_filtered.exomeTarget.sorted.bed - Check '--src' / '-s' input. See help for details. \"Rscript contaminationPrediction_2.R --help\" \n"))
}

if (!file.exists(opt$input)){
  stop(paste0("Cannot locate ", opt$input, " - Check '--input' / '-i'. See help for details. \"Rscript contaminationPrediction.R --help\" \n"))
}
patientList <- read.table(opt$input, stringsAsFactors = F, header = F, sep="\t")

if (ncol(patientList) != 4){
  stop(paste0("Input file: ", opt$input, " - Incorrect file format. See specifications.\n"))
}

files <- as.vector(t(as.matrix(patientList[,2:4])))
fileCheck <- !file.exists(paste0(opt$dir, "/", files))
if (sum(fileCheck) != 0){
  cat("Error: Missing variant files...", paste0(opt$dir, "/", files)[fileCheck], sep = "\n")
  stop('\r      ')
}

if (!dir.exists(opt$out)){
  dir.create(opt$out)
}

if (opt$plot == T & !dir.exists(paste0(opt$out, "/VAF-distribution-plots"))){
  dir.create(paste0(opt$out, "/VAF-distribution-plots"))
}

if (opt$verbose == T){
  cat(paste0(Sys.time(), ": Starting analysis with ", nrow(patientList), " samples.\n"))
}

for (p in 1:nrow(patientList)){
  if (opt$verbose == T){
    cat(paste0(Sys.time(), ": Preparing input files for sample ", p, " of ", nrow(patientList), ": ", patientList[p,1], "\n"))
  }
  vcf <- unlist(strsplit(patientList[p,2], "/"))
  vcf <- vcf[length(vcf)]

  if (grepl(".vcf$", patientList[p,2])){
    system(paste0("bgzip -c ", opt$dir, "/", patientList[p,2], " > ", opt$out, "/", vcf, ".gz"))
    system(paste0("tabix -p vcf ", opt$out, "/", vcf, ".gz"))
    system(paste0("bcftools view -O v -o ", opt$out, "/filtered_", vcf, ".gz -R ", opt$src, "/hg38_filtered.exomeTarget.sorted.bed -f .,PASS -g het ", opt$out, "/", vcf, ".gz"))
    vcf <- paste0(opt$out, "/filtered_", vcf, ".gz")
  } else if (grepl(".vcf.gz$", patientList[p,2])){
    system(paste0("bcftools view -O v -o ", opt$out, "/filtered_", vcf, " -R ", opt$src, "/hg38_filtered.exomeTarget.sorted.bed -f .,PASS -g het ", opt$dir, "/", patientList[p,2]))
    vcf <- paste0(opt$out, "/filtered_", vcf)
  } else {
    stop(paste0(opt$dir, "/", patientList[p,2], " - Incorrect file extension. Expect '.vcf' or '.vcf.gz'\n"))
  }

  system(paste0(opt$ExomeSeqMiner, "/ExomeSeqMiner/bin/CountAllelicReads -Q 10 -d 10 -s ", vcf, " ", opt$dir, "/", patientList[p,3], " > ", opt$out, "/", patientList[p,1], ".normal.depth"), ignore.stderr = T)
  system(paste0(opt$ExomeSeqMiner, "/ExomeSeqMiner/bin/CountAllelicReads -Q 10 -d 10 -s ", vcf, " ", opt$dir, "/", patientList[p,4], " > ", opt$out, "/", patientList[p,1], ".tumour.depth"), ignore.stderr = T)
  system(paste0("bcftools query -f '%CHROM\t%POS\t%REF\t%ALT\n' ", vcf, " > ", opt$out, "/", patientList[p,1], ".inputAlleles.tsv"))

  normal.data <- read.table(paste0(opt$out, "/", patientList[p,1], ".normal.depth"), sep = "\t", header = T, stringsAsFactors = F)
  tumour.data <- read.table(paste0(opt$out, "/", patientList[p,1], ".tumour.depth"), sep = "\t", header = T, stringsAsFactors = F)
  allele.data <- read.table(paste0(opt$out, "/", patientList[p,1], ".inputAlleles.tsv"), sep = "\t",  header = F, stringsAsFactors = F)
  colnames(allele.data) <- c("Chr", "Position", "ref", "alt")

  normal.data$nVAF <- round(normal.data$Bcount / normal.data$Tcount, 3)
  tumour.data$tVAF <- round(tumour.data$Bcount / tumour.data$Tcount, 3)

  patient.data <- merge(normal.data[,c("Chr", "Position", "nVAF")], tumour.data[,c("Chr", "Position", "tVAF")])
  patient.data$Chr <- paste0("chr", patient.data$Chr)

  patient.data <- merge(patient.data, allele.data)
  colnames(patient.data)[1:2] <- c("chr", "pos")
  patient.data <- patient.data[,c("chr", "pos", "ref", "alt", "nVAF", "tVAF")]

  write.table(patient.data, paste0(opt$out, "/", patientList[p,1], ".VAF.tsv"), sep = "\t", quote = F, row.names = F, col.names = T)

  for (i in 1:22){
    tmp.i <- inclist[inclist$chr == paste0("chr", i),]
    tmp.p <- patient.data[patient.data$chr == paste0("chr", i),]

    chr <- merge(tmp.p, tmp.i)

    if (i == 1){
      filtered.patient.data <- chr
    } else {
      filtered.patient.data <- rbind(filtered.patient.data, chr)
    }
  }

  filtered.patient.data <- filtered.patient.data[filtered.patient.data$nVAF != 0 & filtered.patient.data$nVAF != 1,]
  write.table(filtered.patient.data, paste0(opt$out, "/", patientList[p,1], ".filteredVAF.tsv"), sep = "\t", quote = F, row.names = F, col.names = T)
  if (opt$verbose == T){
    cat("                     Complete.\n")
  }
}

fileCheck <- !file.exists(paste0(opt$out, "/", patientList[,1], ".VAF.tsv"))
if (sum(fileCheck) != 0){
  cat("Error: Missing filtered variant files...", paste0(opt$dir, "/", patientList[,1], ".filteredVAF.tsv")[fileCheck], sep = "\n")
  stop('\r      ')
}

if (opt$verbose == T){
  cat(paste0(Sys.time(), ": Input files complete, starting contamination prediction.\n"))
}

testMatrix <- data.frame(patientList[,1])
colnames(testMatrix) <- "ID"
testMatrix[,c(paste0(rep(c("common","rare"), c(25,25)), 1:25), "totalVar", "medianCommonN", "medianRareN", "medianCommonT", "medianRareT", "nVAF_sd", "commonT_diff", "rareT_diff")] <- NaN

lowerN <- seq(0,0.8,0.2)
upperN <- seq(0.2,1,0.2)

lowerT <- seq(0,0.8,0.2)
upperT <- seq(0.2,1,0.2)

for (p in 1:nrow(patientList)){
  testSample <- read.table(paste0(opt$out, "/", testMatrix$ID[p], ".filteredVAF.tsv"), sep = "\t", header = T, stringsAsFactors = F)
  if (sum(c("nVAF", "tVAF", "rare") %in% colnames(testSample)) != 3){
    stop(paste0("Filtered variant file: ", opt$dir, "/", patientList[p,], ".filteredVAF.tsv - Incorrect file format. See specifications.\n"))
  }

  if (opt$plot == T){
    pdf(paste0(opt$out, "/VAF-distribution-plots/",  testMatrix$ID[p], ".pdf"))
    plot(0, 0, type = "n", xlim = c(0,1), ylim = c(0,1), main = testMatrix$ID[p], xlab = "nVAF", ylab = "tVAF")
    points(testSample[testSample$rare == 0, c("nVAF", "tVAF")], col = "grey")
    points(testSample[testSample$rare == 1, c("nVAF", "tVAF")], col = "black")
    abline(v=median(testSample$nVAF[testSample$rare == 0]), lty = 2, lwd = 3, col = "blue")
    abline(v=median(testSample$nVAF[testSample$rare == 1]), lty = 2, lwd = 3, col = "red")
    dev.off()
  }

  commonVector5 <- rep(0,25)
  rareVector5 <- rep(0,25)
  i=0
  for (n in 1:5){
    for (t in 1:5){
      i=i+1
      commonVector5[i] <- sum(testSample$nVAF >= lowerN[n] & testSample$nVAF < upperN[n] & testSample$tVAF >= lowerT[t] & testSample$tVAF < upperT[t] & testSample$rare == 0)
      rareVector5[i] <- sum(testSample$nVAF >= lowerN[n] & testSample$nVAF < upperN[n] & testSample$tVAF >= lowerT[t] & testSample$tVAF < upperT[t] & testSample$rare == 1)
    }
  }

  testMatrix[p,paste0("common", 1:25)] <- commonVector5
  testMatrix[p,paste0("rare", 1:25)] <- rareVector5
  testMatrix$totalVar[p] <- nrow(testSample)

  testMatrix$medianCommonN[p] <- median(testSample$nVAF[testSample$rare == 0])
  testMatrix$medianRareN[p] <- median(testSample$nVAF[testSample$rare == 1])
  testMatrix$medianCommonT[p] <- median(testSample$tVAF[testSample$rare == 0])
  testMatrix$medianRareT[p] <- median(testSample$tVAF[testSample$rare == 1])
  testMatrix$nVAF_sd[p] <- sd(testSample$nVAF)
}

testMatrix$rareT_diff <- rowSums(testMatrix[,paste0("rare",c(6,7,11,12,16,17))]) / rowSums(testMatrix[,paste0("rare",c(9,10,14,15,19,20))])
testMatrix$commonT_diff <- rowSums(testMatrix[,paste0("common",c(6,7,11,12,16,17))]) / rowSums(testMatrix[,paste0("common",c(9,10,14,15,19,20))])

load(paste0(opt$src, "/contaminationPredictionModel.Rdat"))

testCols <- c(paste0("common",c(1:5,21:25)), paste0("rare",c(1:5,21:25)), "medianCommonN", "medianRareN")
test <- testMatrix[,testCols]

if (nrow(patientList) == 1){
  out <- data.frame(cbind(patientList[,1], as.character(predict(contamMod, test)), t(as.data.frame(predict(contamMod, test, type="p")))))
} else {
  out <- data.frame(cbind(patientList[,1], as.character(predict(contamMod, test)), as.data.frame(predict(contamMod, test, type="p"))))
}

row.names(out) <- NULL
colnames(out) <- c("ID", "Prediction", "C0", "C1", "C2", "C3")
out$ID <- as.character(out$ID)
out$Prediction <- as.character(out$Prediction)
for (n in c("C0", "C1", "C2", "C3")){
  out[,n] <- as.numeric(as.character(out[,n]))
}

out$Prediction[testMatrix$totalVar < 10000] <- "X"
out$Prediction[testMatrix$medianCommonN > 0.5] <- "X"
out$Prediction[testMatrix$medianCommonN < 0.4] <- "X"
out$Prediction[testMatrix$nVAF_sd > 0.16] <- "X"
out$Prediction[testMatrix$commonT_diff < 0.4] <- "X"
out$Prediction[(testMatrix$medianCommonT - testMatrix$medianRareT) > 0.015 & testMatrix$rareT_diff > 5] <- "X"
out$Prediction[(testMatrix$medianCommonT - testMatrix$medianRareT) > 0.03 & testMatrix$rareT_diff > 3] <- "X"
out$Prediction[(testMatrix$medianCommonT - testMatrix$medianRareT) > 0.1] <- "X"
out$Prediction[(testMatrix$medianCommonT - testMatrix$medianRareT) < -0.1 & testMatrix$rareT_diff > 1] <- "X"

if (is.null(opt$jobid)){
  file <- paste0(opt$out, "/contaminationPrediction")
} else {
  file <- paste0(opt$out, "/", opt$jobid, ".contaminationPrediction")
}

if (!is.null(opt$batchid)){
  file <- paste0(file, ".", opt$batchid)
}

if (!file.exists(paste0(file, ".txt"))){
  write.table(format(out, digits=3), paste0(file, ".txt"), sep = "\t", quote=F, row.names=F, col.names=T)
  cat(paste0(Sys.time(), ": Output printed to: ", file, ".txt\n"))
} else {
  if (opt$overwrite == T){
    write.table(format(out, digits=3), paste0(file, ".txt"), sep = "\t", quote=F, row.names=F, col.names=T)
    cat(paste0(Sys.time(), ": Overwriting existing file. Output printed to: ", file, ".txt\n"))
  } else if (opt$append == T){
    write.table(format(out, digits=3), paste0(file, ".txt"), sep = "\t", quote=F, row.names=F, col.names=F, append=T)
    cat(paste0(Sys.time(), ": Appending to existing file. Output printed to: ", file, ".txt\n"))
  } else {
    n=1
    while (file.exists(paste0(file, ".", n, ".txt"))){
      n=(n+1)
    }
    write.table(format(out, digits=3), paste0(file, ".", n, ".txt"), sep = "\t", quote=F, row.names=F, col.names=F, append=T)
    cat(paste0(Sys.time(), ": ", file, ".txt already exists. Output printed to: ", file, ".", n, ".txt\n"))
  }
}

end <- Sys.time()

if (opt$verbose == T){
  cat(paste0(Sys.time(), ": Analysis complete.\n"))
  cat("                     Total time elapsed:", format(end - start, digits = 3), "\n")
  cat(paste0("                     Total samples analysed: ", nrow(out), "\n                     C0=", sum(out$Prediction == 'C0'), " C1=", sum(out$Prediction == 'C1'), " C2=", sum(out$Prediction == 'C2'), " C3=", sum(out$Prediction == 'C3'), " X=", sum(out$Prediction == 'X'), "\n"))
  if (opt$plot == T){
    cat(paste0("                     Per-sample VAF distribution plots in: ", opt$out, "/VAF-distribution-plots/\n"))
  }
}
